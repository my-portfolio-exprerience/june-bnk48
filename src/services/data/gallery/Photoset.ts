module.exports = {
  photoset: [
    {
      name: '[Debut] - Cover A',
      image: {
        name: 'gallery%2Fphotoset%2Fdebut%20(cover%20A)'
      }
    },
    {
      name: '[Debut] - Cover B',
      image: {
        name: 'gallery%2Fphotoset%2Fdebut%20(cover%20B)'
      }
    },
    {
      name: '[Debut] - Cover C',
      image: {
        name: 'gallery%2Fphotoset%2Fdebut%20(cover%20C)'
      }
    },
    {
      name: '[Debut] - Closeup',
      image: {
        name: 'gallery%2Fphotoset%2Fdebut%20(closeup)'
      }
    },
    {
      name: '[Debut] - Half',
      image: {
        name: 'gallery%2Fphotoset%2Fdebut%20(half)'
      }
    },
    {
      name: '[Debut] - Full',
      image: {
        name: 'gallery%2Fphotoset%2Fdebut%20(full)'
      }
    },
    {
      name: '[Single 4] - Tsugi no Season (Cover)',
      image: {
        name: 'gallery%2Fphotoset%2Ftsugi%20no%20season%20(cover)'
      }
    },
    {
      name: '[Single 4] - Kimi wa melody (Closeup)',
      image: {
        name: 'gallery%2Fphotoset%2Fkimi%20wa%20melody%20(closeup)'
      }
    },
    {
      name: '[Single 4] - Kimi wa melody (Half)',
      image: {
        name: 'gallery%2Fphotoset%2Fkimi%20wa%20melody%20(half)'
      }
    },
    {
      name: '[Single 4] - Kimi wa melody (Full)',
      image: {
        name: 'gallery%2Fphotoset%2Fkimi%20wa%20melody%20(full)'
      }
    },
    {
      name: '[Single 5] - BNK Festival (Cover)',
      image: {
        name: 'gallery%2Fphotoset%2Fbnk%20festival%20(cover)'
      }
    },
    {
      name: '[Single 5] - BNK Festival (Closeup)',
      image: {
        name: 'gallery%2Fphotoset%2Fbnk%20festival%20(closeup)'
      }
    },
    {
      name: '[Single 5] - BNK Festival (Half)',
      image: {
        name: 'gallery%2Fphotoset%2Fbnk%20festival%20(half)'
      }
    },
    {
      name: '[Single 5] - BNK Festival (Full)',
      image: {
        name: 'gallery%2Fphotoset%2Fbnk%20festival%20(full)'
      }
    },
    {
      name: '[General Election] - Cover',
      image: {
        name: 'gallery%2Fphotoset%2Fgeneral%20election%20(cover)'
      }
    },
    {
      name: '[General Election] - Closeup',
      image: {
        name: 'gallery%2Fphotoset%2Fgeneral%20election%20(closeup)'
      }
    },
    {
      name: '[General Election] - Half',
      image: {
        name: 'gallery%2Fphotoset%2Fgeneral%20election%20(half)'
      }
    },
    {
      name: '[General Election] - Full',
      image: {
        name: 'gallery%2Fphotoset%2Fgeneral%20election%20(full)'
      }
    },
    {
      name: '[General Election] - Photobook',
      image: {
        name: 'gallery%2Fphotoset%2Fgeneral%20election%20(photobook)'
      }
    },
    {
      name: '[Single 6] - Kimi no koto ga suki dakara (Senbutsu)',
      image: {
        name: 'gallery%2Fphotoset%2FKimi%20no%20suki%20dakara%20(senbutsu)'
      }
    },
    {
      name: '[Single 6] - Kimi no koto ga suki dakara (MV)',
      image: {
        name: 'gallery%2Fphotoset%2FKimi%20no%20suki%20dakara%20(mv)'
      }
    },
    {
      name: '[Single 6] - Beginner (Closeup)',
      image: {
        name: 'gallery%2Fphotoset%2Fbeginner%20(closeup)'
      }
    },
    {
      name: '[Single 6] - Beginner (Half)',
      image: {
        name: 'gallery%2Fphotoset%2Fbeginner%20(half)'
      }
    },
    {
      name: '[Single 6] - Beginner (Full)',
      image: {
        name: 'gallery%2Fphotoset%2Fbeginner%20(full)'
      }
    },
    {
      name: '[Special] - Gentle Wowan (Closeup)',
      image: {
        name: 'gallery%2Fphotoset%2Fgentel%20woman%20(closeup)'
      }
    },
    {
      name: '[Special] - Gentle Wowan (Half)',
      image: {
        name: 'gallery%2Fphotoset%2Fgentel%20woman%20(half)'
      }
    },
    {
      name: '[Special] - Gentle Wowan (Full)',
      image: {
        name: 'gallery%2Fphotoset%2Fgentel%20woman%20(full)'
      }
    },
    {
      name: '[Special] - BNK48 Aniversary 2 years',
      image: {
        name: 'gallery%2Fphotoset%2Faniversary%202%20years'
      }
    },
    {
      name: '[2nd album] - JABAJA (Cover)',
      image: {
        name: 'gallery%2Fphotoset%2Fjabaja%20(cover)'
      }
    },
    {
      name: '[2nd album] - JABAJA (Closeup)',
      image: {
        name: 'gallery%2Fphotoset%2Fjabaja%20(closeup)'
      }
    },
    {
      name: '[2nd album] - JABAJA (Half)',
      image: {
        name: 'gallery%2Fphotoset%2Fjabaja%20(half)'
      }
    },
    {
      name: '[2nd album] - JABAJA (Full)',
      image: {
        name: 'gallery%2Fphotoset%2Fjabaja%20(full)'
      }
    },
    {
      name: '[Special] - เทศกาลกีฬาบางกอก ๔๘ (Closeup)',
      image: {
        name: 'gallery%2Fphotoset%2Fsport%20day%20(closeup)'
      }
    },
    {
      name: '[Special] - เทศกาลกีฬาบางกอก ๔๘ (Half)',
      image: {
        name: 'gallery%2Fphotoset%2Fsport%20day%20(half)'
      }
    },
    {
      name: '[Special] - เทศกาลกีฬาบางกอก ๔๘ (Full)',
      image: {
        name: 'gallery%2Fphotoset%2Fsport%20day%20(full)'
      }
    },
    {
      name: '[Special] - Photobook The frozer (BNK48 2nd generation)',
      image: {
        name: 'gallery%2Fphotoset%2Fphotobook%20(2nd%20generation)'
      }
    },
    {
      name: '[Special] - Blooming season concert (Closeup)',
      image: {
        name: 'gallery%2Fphotoset%2Fblooming%20season%20(closeup)'
      }
    },
    {
      name: '[Special] - Blooming season concert (Half)',
      image: {
        name: 'gallery%2Fphotoset%2Fblooming%20season%20(half)'
      }
    },
    {
      name: '[Special] - Blooming season concert (Full)',
      image: {
        name: 'gallery%2Fphotoset%2Fblooming%20season%20(full)'
      }
    },
    {
      name:
        '[Single 7] - 77 ดินแดนแสนวิเศษ | 77 no suteki na machi e (Special 7 - 8 Dec 2019)',
      image: {
        name:
          'gallery%2Fphotoset%2F77%20no%20suteki%20na%20machi%20e%20(special%207%20-%208%20Dec%202019)'
      }
    },
    {
      name:
        '[Single 7] - 77 ดินแดนแสนวิเศษ | 77 no suteki na machi e (Special 15 - 16 Feb 2020)',
      image: {
        name:
          'gallery%2Fphotoset%2F77%20no%20suteki%20na%20machi%20e%20(special%2015%20-%2016%20Feb%202020)'
      }
    },
    {
      name: '[Single 7] - 77 ดินแดนแสนวิเศษ | 77 no suteki na machi e (Cover)',
      image: {
        name: 'gallery%2Fphotoset%2F77%20no%20suteki%20na%20machi%20e%20(cover)'
      }
    },
    {
      name:
        '[Single 7] - 77 ดินแดนแสนวิเศษ | 77 no suteki na machi e (Closeup)',
      image: {
        name:
          'gallery%2Fphotoset%2F77%20no%20suteki%20na%20machi%20e%20(closeup)'
      }
    },
    {
      name: '[Single 7] - 77 ดินแดนแสนวิเศษ | 77 no suteki na machi e (Half)',
      image: {
        name: 'gallery%2Fphotoset%2F77%20no%20suteki%20na%20machi%20e%20(half)'
      }
    },
    {
      name: '[Single 7] - 77 ดินแดนแสนวิเศษ | 77 no suteki na machi e (Full)',
      image: {
        name: 'gallery%2Fphotoset%2F77%20no%20suteki%20na%20machi%20e%20(full)'
      }
    },
    {
      name: '[Single 8] - High Tension (Cover)',
      image: {
        name: 'gallery%2Fphotoset%2Fhigh%20tension%20(cover)'
      }
    },
    {
      name: '[Single 8] - High Tension (Closeup)',
      image: {
        name: 'gallery%2Fphotoset%2Fhigh%20tension%20(closeup)'
      }
    },
    {
      name: '[Single 8] - High Tension (Half)',
      image: {
        name: 'gallery%2Fphotoset%2Fhigh%20tension%20(half)'
      }
    },
    {
      name: '[Single 8] - High Tension (Full)',
      image: {
        name: 'gallery%2Fphotoset%2Fhigh%20tension%20(full)'
      }
    },
    {
      name: '[General Election II] - Cover',
      image: {
        name: 'gallery%2Fphotoset%2Fgeneral%20election%20II%20%20(cover)'
      }
    },
    {
      name: '[General Election II] - Closeup',
      image: {
        name: 'gallery%2Fphotoset%2Fgeneral%20election%20II%20(closeup)'
      }
    },
    {
      name: '[General Election II] - Half',
      image: {
        name: 'gallery%2Fphotoset%2Fgeneral%20election%20II%20(half)'
      }
    },
    {
      name: '[General Election II] - Full',
      image: {
        name: 'gallery%2Fphotoset%2Fgeneral%20election%20II%20(full)'
      }
    },
    {
      name: '[General Election 2] - Photobook',
      image: {
        name: 'gallery%2Fphotoset%2Fgeneral%20election%202%20(photobook)'
      }
    },
    {
      name: '[Single 9] - Heavy Rotation (Cover I)',
      image: {
        name: 'gallery%2Fphotoset%2Fheavy%20rotation%20(cover%20I)'
      }
    },
    {
      name: '[Single 9] - Heavy Rotation (Cover II)',
      image: {
        name: 'gallery%2Fphotoset%2Fheavy%20rotation%20(cover%20II)'
      }
    },
    {
      name: '[Single 9] - Heavy Rotation (Cover III)',
      image: {
        name: 'gallery%2Fphotoset%2Fheavy%20rotation%20(cover%20III)'
      }
    },
    //     {
    //       name: '[Single 9] - Heavy Rotation (Closeup)',
    //       image: {
    //         name: ''
    //       }
    //     },
    //     {
    //       name: '[Single 9] - Heavy Rotation (Half)',
    //       image: {
    //         name: ''
    //       }
    //     },
    {
      name: '[Single 9] - Heavy Rotation (Full)',
      image: {
        name: 'gallery%2Fphotoset%2Fheavy%20rotation%20(full)'
      }
    }
  ]
}
