module.exports = {
  socials: [
    {
      name: 'Facebook',
      image: {
        name: 'base%2Flogo%2Ffacebook'
      }
    },
    {
      name: 'Instagram',
      image: {
        name: 'base%2Flogo%2Finstagram'
      }
    },
    {
      name: 'LINE Square (SQ)',
      image: {
        name: 'base%2Flogo%2Flinesquare'
      }
    },
    {
      name: 'Twitter',
      image: {
        name: 'base%2Flogo%2Ftwitter'
      }
    },
    {
      name: 'Youtube Chanel',
      image: {
        name: 'base%2Flogo%2Fyoutube'
      }
    },
    {
      name: 'Facebook Group',
      image: {
        name: 'base%2Flogo%2Ffacebookgroup'
      }
    },
    {
      name: 'Facebook Page',
      image: {
        name: 'base%2Flogo%2Ffacebookpage'
      }
    }
  ],
  june: {
    image: {
      name: 'followup%2Fjune%2Ffollowup'
    },
    contact: [
      {
        name: 'Facebook',
        link: {
          name: 'Juné BNK48',
          route: 'https://www.facebook.com/bnk48official.june/'
        }
      },
      {
        name: 'Instagram',
        link: {
          name: 'june.bnk48office',
          route: 'https://www.instagram.com/june.bnk48office/'
        }
      },
      {
        name: 'Instagram',
        link: {
          name: 'juneiperb0i',
          route: 'https://www.instagram.com/juneiperb0i/'
        }
      }
    ],
    hashtag: [
      {
        name: 'junébnk48',
        route: 'https://www.instagram.com/explore/tags/jun%C3%A9bnk48/'
      },
      {
        name: 'filmkongjń',
        route: 'https://www.instagram.com/explore/tags/filmkongj%C5%84/'
      },
      {
        name: 'taiidbabnitongdetoxbyjuné',
        route:
          'https://www.instagram.com/explore/tags/taiidbabnitongdetoxbyjun%C3%A9/'
      },
      {
        name: 'jumykheng',
        route: 'https://www.instagram.com/explore/tags/jumykheng/?hl=en'
      }
    ]
  },
  groups: [
    {
      name: 'ร้านบะหมี่ของจูเน่',
      image: {
        name: 'followup%2Fgroups%2Fร้านบะหมี่ของจูเน่'
      },
      contact: [
        {
          name: 'Facebook',
          link: {
            name: 'Juné BNK48 Thailand Fanclub',
            route: 'https://www.facebook.com/JuneTHfanclub/'
          }
        },
        {
          name: 'Instagram',
          link: {
            name: 'june.bnk48supporter',
            route: 'https://www.instagram.com/june.bnk48supporter/'
          }
        },
        {
          name: 'LINE Square (SQ)',
          link: {
            name: '#ร้านบะหมี่ของจูเน่',
            route: 'https://line.me/ti/g2/Nm07y4gVxlh_MVENV-VqEw'
          }
        },
        {
          name: 'Twitter',
          link: {
            name: '@JuneBnk48FC',
            route: 'https://twitter.com/JuneBnk48FC?s=09'
          }
        },
        {
          name: 'Youtube Chanel',
          link: {
            name: 'Juné BNK48 Thailand Fanclub',
            route: 'https://www.youtube.com/channel/UCrqDo51ZS6OP6HnidoEUmCg'
          }
        }
      ]
    },
    {
      name: 'Juné Family',
      image: {
        name: 'followup%2Fgroups%2Fjune%20family'
      },
      contact: [
        {
          name: 'Facebook',
          link: {
            name: 'Juné BNK48 Family',
            route: 'https://www.facebook.com/junebnk48family/'
          }
        },
        {
          name: 'Instagram',
          link: {
            name: 'june.family',
            route: 'https://www.instagram.com/june.family/'
          }
        },
        {
          name: 'LINE Square (SQ)',
          link: {
            name: 'Juné BNK48 Family',
            route: 'https://line.me/ti/g2/FBRPRDF4A4'
          }
        },
        {
          name: 'Twitter',
          link: {
            name: '@june_family',
            route: 'https://twitter.com/june_family?s=06'
          }
        }
      ]
    },
    {
      name: 'Juniversé',
      image: {
        name: 'followup%2Fgroups%2Fjuniverse'
      },
      contact: [
        {
          name: 'Facebook',
          link: {
            name: 'Juniversé - Juné BNK48 Fanclub',
            route: 'https://www.facebook.com/Juniverse.JuneBNK48/'
          }
        },
        {
          name: 'Instagram',
          link: {
            name: 'juniverse48',
            route: 'https://www.instagram.com/juniverse48/'
          }
        },
        {
          name: 'LINE Square (SQ)',
          link: {
            name: 'Juniversé - Juné BNK48',
            route: 'https://line.me/ti/g2/ECADO9PAP9'
          }
        },
        {
          name: 'Twitter',
          link: {
            name: '@juniverse48',
            route: 'https://twitter.com/juniverse48'
          }
        },
        {
          name: 'Facebook Group',
          link: {
            name: 'Juniversé - Juné BNK48 Fanclub Community',
            route: 'https://m.facebook.com/groups/1973194156323850?ref=share'
          }
        }
      ]
    },
    {
      name: 'JUst NÉ - Juné BNK48',
      image: {
        name: 'followup%2Fgroups%2FJust%20NÉ%20-%20Juné%20BNK48'
      },
      contact: [
        {
          name: 'LINE Square (SQ)',
          link: {
            name: 'JUst NÉ - Juné BNK48',
            route:
              'https://line.me/ti/g2/0TDCo0yyRGbgu0UU0qBmaA?utm_source=invitation&utm_medium=QR_code&utm_campaign=default'
          }
        }
      ]
    }
  ]
}
