module.exports = {
  menu: [
    {
      name: 'About Juné',
      value: 'about_june',
      route: '/about_june',
      icon: 'emoticon-wink-outline'
    },
    {
      name: 'Schedule Event',
      value: 'schedule_event',
      route: '/schedule_event',
      icon: 'calendar'
    },
    {
      name: 'Performance',
      value: 'performance',
      route: '/performance',
      icon: 'theater'
    },
    {
      name: 'Gallery',
      value: 'gallery',
      route: '/gallery',
      icon: 'film'
    },
    {
      name: 'Songs',
      value: 'song',
      route: '/song',
      icon: 'piano'
    },
    // {
    //   name: 'Shop',
    //   value: 'shop',
    //   route: '/shop',
    //   icon: 'cart'
    // },
    {
      name: 'Follow up',
      value: 'follow_up',
      route: '/follow_up',
      icon: 'thumb-up'
    }
  ],
  contact: [
    {
      name: 'base%2Flogo%2Ffacebook',
      detail: {
        title: 'Facebook Page',
        description: 'mukashi1911',
        link: 'https://www.facebook.com/mukashi1911'
      }
    },
    {
      name: 'base%2Flogo%2Finstagram',
      detail: {
        title: 'Instagram',
        description: 'mukashi1911',
        link: 'https://www.instagram.com/mukashi1911'
      }
    },
    {
      name: 'base%2Flogo%2Ftwitter',
      detail: {
        title: 'Twitter',
        description: '@mukashi1911',
        link: 'https://twitter.com/mukashi1911'
      }
    }
  ],
  img_sidebar: [
    { name: 'base%2Fsidebar%2Fsidebar-001' },
    { name: 'base%2Fsidebar%2Fsidebar-002' },
    { name: 'base%2Fsidebar%2Fsidebar-003' },
    { name: 'base%2Fsidebar%2Fsidebar-004' },
    { name: 'base%2Fsidebar%2Fsidebar-005' },
    { name: 'base%2Fsidebar%2Fsidebar-006' }
  ]
}
