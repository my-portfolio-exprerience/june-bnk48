module.exports = {
  cover: {
    name: 'gallery%2Fcover'
  },
  tabs: [
    { name: 'Photoset', value: 'photoset' },
    // { name: 'Event', value: 'event' },
    { name: 'Fan art', value: 'fan_art' }
  ]
}
